import static org.junit.Assert.*;

import org.junit.Test;

public class TestCasesforBank {

	@Test
	public void Testnumberofcustomers() {
		Bank b= new Bank();    
		//To Test newly created bank has zero customers
		assertEquals(0,b.getNumberOfCustomers());
	}
	
	@Test
	public void TestAddCustomers() {
		Bank b= new Bank();
		//To Test if you can add customers to bank.The number of customers increases by 1
		b.addCustomer("billy", 100);
	assertEquals(1,b.getNumberOfCustomers());
	}
	
	@Test
	public void TestDeleteCustomers() {
		Bank b= new Bank();
		//To Test if you can delete customers from bank.The number of customers decreases by 1
		b.addCustomer("billy", 100);
	b.removeCustomer("billy");
	assertEquals(0,b.getNumberOfCustomers());
	}
	
	@Test
	public void TestTransferMoneyandBalanceCheck() {
		Bank b= new Bank();
		//To Test if you tranfer funds between two customers and balance is updated in both the accounts
		
		b.addCustomer("billy", 500);
		b.addCustomer("jimmy", 200);
	   Customer billy=b.getCustomers().get(0);
	   Customer jimmy=b.getCustomers().get(1);
	   b.transferMoney(billy, jimmy, 200); 
	   assertEquals(300,billy.getAccount().balance());
	   //Balance of customer1 and customer2 are updated which means withdrawl works.
	   assertEquals(400,jimmy.getAccount().balance());
			}
	
	@Test
	public void TestTransferMoneyusingNegativeValues() {
		Bank b= new Bank();
		// Test if you tranfer funds between two customers and balance is updated in both the accounts using negative amount
		b.addCustomer("billy", 500);
		b.addCustomer("jimmy", 200);
	   Customer billy=b.getCustomers().get(0);
	   Customer jimmy=b.getCustomers().get(1);
	   b.transferMoney(billy, jimmy, -200); 
	   assertEquals(500,billy.getAccount().balance());
	   //Balance of customer1 and customer2 are updated which means withdrawl works.
	   assertEquals(200,jimmy.getAccount().balance());
			}
	
	@Test
	public void TestAddCustomersusingNegativeDeposit() {
		Bank b= new Bank();
		//To Test if you can add customers to bank using negative deposit.The number of customers increases by 1
		b.addCustomer("billy", 100);
	assertEquals(1,b.getNumberOfCustomers());
	}
}
