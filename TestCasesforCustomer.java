import static org.junit.Assert.*;

import org.junit.Test;

public class TestCasesforCustomer {

	@Test
	public void TestnumberofcustomersforinitialDeposit() 
	{
			//Test to see if optional deposit of 0 is accepted or not
			Customer c= new Customer("billy",0);
			 Account account=	c.getAccount();
		assertEquals(0,account.balance());
	}	
	@Test
	public void TestnumberofcustomersforDeposit() 
	{
			//Test to see if deposit of 100 is accepted or not
			Customer c= new Customer("billy",100);
			 Account account=	c.getAccount();
		assertEquals(100,account.balance());
	}
	
	@Test
	public void TestCustomergetAccount() {
		
		Customer c= new Customer("billy",100);
		//Test to see if everycustomer has account
		 Account account=	c.getAccount();
	assertEquals(100,account.balance());//since balance is same the case is passed
	}
	
	@Test
	public void TestCustomergetAccountforNegatibveValues() {
		
		Customer c= new Customer("billy",-100);
		//Test to see if customer deposit is negative
		 Account account=	c.getAccount();
	assertNotEquals(0,account.balance());
	}
}

