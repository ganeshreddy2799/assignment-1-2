import static org.junit.Assert.*;

import org.junit.Test;

public class TestCasesForAccount {
	
	@Test
	public void TestInitialBalance() {
	//Test to see All new accounts are initialized with 0 dollars in the account
	Account a=new Account(100);
	assertEquals(0,a.balance());
	}
	
	@Test
	public void TestWithdraw() {
  //Test to see if you can  withdraw money from an account and balance is updated immediately 
	Account a=new Account(100);
	a.withdraw(90);
	assertEquals(10,a.balance());
	}
	
	@Test
	public void TestOverDraft() {
    //Test to see if withdrawl works only when sufficient funds are avaliable
	Account a=new Account(200);
	a.deposit(100);
	a.withdraw(500);
				}
	@Test
	public void TestWithdrawforNegativeValue() {
  //Test to see if you can  withdraw money from an account for negative value 
	Account a=new Account(100);
	a.withdraw(-90);
	assertEquals(10,a.balance());
}
	
	@Test
	public void TestDepositForNegativeBalance()  {
	//Test to see if you can deposit Negative money in acccount.
	Account a=new Account(0);
	a.deposit(-100);
	assertEquals(100,a.balance());
	}
}

